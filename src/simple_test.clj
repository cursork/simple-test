(ns simple-test
  "An insanely simple testing library, inspired by Perl's testing libraries.

  At the heart of simple-test is one function, `ok`:

    (ok true  \"true is true\")        ;; Pass
    (ok false \"true is true\")        ;; Fail
    (ok true)                        ;; Comment not required
    (ok #(do (println \"Hello World\") ;; Execute function as test
             (= 1 2)))

  A namespace can be tested using `(test-ns 'some-namespace)`.

  A directory can be tested using `(test-directory \"test\")`.

  N.B. it is assumed all tests for one namespace are run in the same thread
  (although multiple threads for multiple namespaces, running in parallel, is
  OK)."
  (:require [clojure.tools.namespace.find :as ns-find]
            [cemerick.pomegranate :as grenatapfel]
            [simple-test.internal :refer :all])
  (:import [java.io File StringWriter]))

(defn ok
  "Takes a value or a 0-arity function. If a value, passes or fails the test
  based on the truthiness of the value. If a function, evaluates the function
  and passes or fails the test appropriately.

  The function form is preferred in many cases; it will capture exceptions and
  fail the test."
  ([v-or-fn] (ok v-or-fn nil))
  ([v-or-fn text]
   (if-let [v (if (ifn? v-or-fn)
                (try (v-or-fn) (catch Exception _ nil))
                v-or-fn)]
     (pass text)
     (fail text))))

(defn test-ns
  "Tests a namespace and returns the summary of results."
  [to-test-ns]
  (eval-in-test-context #(require to-test-ns :reload-all)))

(defn test-directory
  "Test all namespaces in the given directory and return a summary of all
  tests."
  [dir]
  (let [dirf (File. dir)
        _    (grenatapfel/add-classpath dirf)
        summaries (into
                    {}
                    (map (fn [n] [n (test-ns n)])
                         (ns-find/find-namespaces-in-dir dirf)))]
    (merge-summaries summaries)))

