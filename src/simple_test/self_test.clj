(ns simple-test.self-test
  (:require simple-test
            simple-test.format)
  (:gen-class))

(defn -main
  [& args]
  (let [res (simple-test/test-directory "test")]
    (simple-test.format/print-coloured-all res)
    (System/exit (count (:failed-namespaces res)))))

