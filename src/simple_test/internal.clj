(ns simple-test.internal
  (:import [java.io StringWriter]))

(def ^:dynamic *testing*    nil)
(def ^:dynamic *pass-fails* nil)

(defn pass
  "Simply records a pass, with optional text."
  ([] (pass nil))
  ([text]
   (when *testing*
     (swap! *pass-fails* conj [:pass text]))))

(defn fail
  "Simply records a fail, with optional text."
  ([] (fail nil))
  ([text]
   (when *testing*
     (swap! *pass-fails* conj [:fail text]))))

(defn summarise
  "Internal use. Summarises the collected pass and fail test results."
  [pass-fails]
  (let [numbered     (let [i (atom 0)]
                       (map #(conj % (swap! i inc)) pass-fails))
        passed-tests (filter #(= :pass (first %)) numbered)
        failed-tests (filter #(= :fail (first %)) numbered)]
    {:passed-tests passed-tests
     :failed-tests failed-tests
     :num-passed   (count passed-tests)
     :num-failed   (count failed-tests)
     :success?     (= 0 (count failed-tests))}))

(defn merge-summaries
  [summaries]
  (let [passed-namespaces (->> summaries
                               (filter (fn [[n s]] (:success? s)))
                               (map first))
        failed-namespaces (->> summaries
                               (remove (fn [[n s]] (:success? s)))
                               (map first))
        total-passes (some->> summaries
                              vals
                              (map :num-passed)
                              (reduce + 0))
        total-fails  (some->> summaries
                              vals
                              (map :num-failed)
                              (reduce + 0))
        ]
    {:passed-namespaces passed-namespaces
     :failed-namespaces failed-namespaces
     :total-passes      total-passes
     :total-fails       total-fails
     :success?          (= 0 total-fails)
     :summaries         summaries}))

(defn eval-in-test-context
  [f]
  (binding [*testing*    true
            *pass-fails* (atom [])
            *out*        (StringWriter.)
            *err*        (StringWriter.)]
    (f)
    (-> @*pass-fails*
        summarise
        (assoc :out (str *out*) :err (str *err*)))))

(defmacro test-code
  "Internal use. Macro to test arbitrary code. Used by simple-test for its own
  unit tests."
  [& body]
  `(eval-in-test-context (fn [] ~@body)))

