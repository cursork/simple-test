(ns simple-test.experimental
  "A namespace for any experimental features for simple-test, that are not yet
  part of the official API, but may be used in your tests.

  A supremely cautious approach is taken for simple-test. As such, no functions
  or macros become a part of it until they've been proven to be actively used
  \"in the wild\".

  This namespace exists to act as a form of explicit \"documentation\" that
  nothing in here is guaranteed between versions. This is unlike the
  simple-test namespace itself, which may only ever change - with a deprecation
  cycle - at major version boundaries."
  (:require [simple-test.internal :as internal :refer (pass fail test-code)]))

(defn throws
  "Evaluates an anonymous function and passes if the given exception class was
  thrown."
  ([f expected] (throws f expected nil))
  ([f expected text]
   (if-let [success (try
                      (f)
                      false
                      (catch Exception e
                        (instance? expected e)))]
     (pass text)
     (fail text))))

(defmacro test-set
  "Only evaluates body if *testing* is true. Useful for those people who
  really, really do want to require namespaces without running any code.

  EXPERIMENTAL."
  [& body]
  `(if internal/*testing*
     ((fn [] ~@body))
     nil))

