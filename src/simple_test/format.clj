(ns simple-test.format
  (:require [clansi.core :as clansi]))

(defn print-coloured-one
  [ns-summary]
  (let [{:keys [err out passed-tests failed-tests num-passed num-failed success?]}
         ns-summary

         all-tests (sort-by #(nth % 2) (concat passed-tests failed-tests))]
    (when (seq out)
      (println (str (clansi/style "Standard Out:\n" :cyan) out)))
    (when (seq err)
      (println (str (clansi/style "Standard Error:\n" :cyan) err)))
    (doseq [t all-tests]
      (println (clansi/style (if (seq (second t)) (second t) (str "Test " (nth t 2)))
                             :bright
                             (if (= :pass (first t))
                               :green
                               :red))))))

(defn print-coloured-all
  [summary]
  (let [{:keys [passed-namespaces failed-namespaces total-passes total-fails success? summaries]}
        summary]
    (doseq [[nsname ns-summary] summaries]
      (println (str \newline (clansi/style (str "==== " nsname " ====") :bright :blue) \newline))
      (print-coloured-one ns-summary))))

