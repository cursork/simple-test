# TODO

Vaguely in order of my preferences

  * some kind of auto-testing
  * allow watching of tests as they happen (rather than waiting)
  * pretty-ish summary view for tests
    * could also support JUnit, TAP, whatever-is-cool-nowadays
  * fail-fast mode? i.e. autotest but abort at first failure - I do not
    enjoy scrolling back to find the first failure
    i.e. keep terminal auto-testing in background and always show first
    failure in it - my tests tend to build incrementally on each other and
    I generally always fix them from first failure forwards
  * copy more ideas from Test::More, et al. into the experimental ns
  * decide whether explicitly not checking for correct *ns* inside pass/fail
    functions is desirable (or vice versa). For the moment leave my
    currently desired behaviour (not checking) implicit
  * simple-test.macros with macro versions that:
    * print out the lexical scope for debugging?
    * compile away when not explicitly testing?

