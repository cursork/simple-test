(ns simple-test.experimental-test
  (:require [simple-test              :refer :all]
            [simple-test.experimental :refer (test-set throws)]
            [simple-test.internal     :refer (test-code)]))

(ok #(= (binding [simple-test.internal/*testing* false] (test-set (throw (Exception. "boom"))))
        nil)
    "test-set returns nil and doesn't execute body if *testing* is false")

(ok #(= (binding [simple-test.internal/*testing* true] (test-set "HELLO"))
        "HELLO")
    "test-set returns result of execution if *testing* is true")

(ok (= (-> (test-code (throws #(throw (ex-info "1" {})) clojure.lang.ExceptionInfo "1")
                      (throws #(throw (Exception. "2")) clojure.lang.ExceptionInfo "2"))
           (select-keys [:passed-tests :failed-tests]))
       {:passed-tests [[:pass "1" 1]]
        :failed-tests [[:fail "2" 2]]})
    "throws captures and compares exceptions")

#_(dissoc (test-ns (ns-name *ns*))
          :passed-tests)
