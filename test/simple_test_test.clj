(ns simple-test-test
  (:require [simple-test          :refer :all]
            [simple-test.internal :refer (test-code)]))

;; Testing simple-test, with simple-test!

(ok (= 1 (:num-failed (test-code (ok nil))))
    "ok nil is a failure")

(ok (= 1 (:num-passed (test-code (ok "truthy"))))
    "ok truthy is a success")

(let [summary (test-code (ok true)
                         (ok nil)
                         (ok true)
                         (ok true)
                         (ok nil))]
  (ok (and (= 3 (:num-passed summary))
           (= 2 (:num-failed summary)))
      "Multiple passes and fails counted correctly"))

(ok (= 1 (-> (ok #(throw (Exception.)))
             test-code
             :num-failed))
    "Throwing an exception fails the test")

(ok #(= "hello from *out*\n"
        (:out (test-code (ok (fn [] (println "hello from *out*") true)))))
    "Printing to *out* is captured")

(ok #(= "hello from *err*\n"
        (:err (test-code (ok (fn []
                               (binding [*out* *err*]
                                 (println "hello from *err*")) true)))))
    "Printing to *err* is captured")

;; DIY testing for the moment
#_(dissoc (test-ns (ns-name *ns*))
          :passed-tests)
