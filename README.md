# simple-test

[![Clojars Project](http://clojars.org/simple-test/latest-version.svg)](http://clojars.org/simple-test)

[![wercker status](https://app.wercker.com/status/2a959766d2325768790e403ac8b4a9e2/m/master "wercker status")](https://app.wercker.com/project/bykey/2a959766d2325768790e403ac8b4a9e2)

simple-test aims to be the simplest possible testing solution in Clojure. As
such, it doesn't aim to be complete, but rather to provide a substrate on which
higher-level testing idioms can be built.

**To be honest**, this is a massive yak shaving exercise. You probably don't
want to use this, unless you really really like zero-frills testing.

# Running the Tests

`lein run` will run the library's own tests against itself, using itself.

    lein run

## License

Copyright © 2014 Neil Kirsopp

Distributed under the MIT License.

